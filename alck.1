.\" This file is part of ALCK -*- nroff -*-
.\" Copyright (C) 2013, 2024 Sergey Poznyakoff
.\"
.\" Alck is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Alck is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with Alck.  If not, see <http://www.gnu.org/licenses/>.
.TH ALCK 1 "June 7, 2024" "ALCK"
.SH NAME
alck \- check MTA alias files
.SH SYNOPSIS
\fBalck\fR [\fB\-d\fR \fILEVEL\fR] [\fB\-T\fR \fIFILE\fR]\
 [\-\fBruv\fR] [\fB\-w\fR \fIFILE\fR] \fIFILE\fR
.sp
\fBalck\fR \fB\-h\fR
.sp
\fBalck\fR \fB\-V\fR
.SH DESCRIPTION
.B Alck
checks one or several
.BR sendmail -style
alias files for consistency.  The following tests are performed:
.TP
.B Multiply defined aliases
.TP
.B Transitivity test
This test discovers eventual circular dependencies.
.TP
.B Use of prohibited aliases
This test is optional.  When enabled, it detects the use of pipes,
file redirects and includes in the input files.
.PP
The program returns 0 if all tests pass successfully. Otherwise,
it diagnoses encountered problems and exits with error code 1.
.PP
The program takes a list of alias files to be checked from its command
line.  Command line options can be interspersed with filename
arguments.  The command line option
.B \-T
can be used to read the list of file names from a plain-text file.
Such a file must contain one file name per line.  Empty lines and
lines beginning with
.B #
are ignored.  Any file name that does not begin with a
.B /
is searched in the same directory where the list file resides.  For
example, assuming that the file
.B /etc/mailman/LIST contains:
.sp
.nf
.in +2
mailman
mailman-test
.in
.fi
.sp
the following invocation
.sp
.nf
.in +2
$ alck /etc/mail/aliases -T/etc/mailman/LIST
.in
.fi
.sp
would instruct
.B alck
to process files
.BR /etc/mail/aliases ,
.B /etc/mailman/mailman
and
.BR /etc/mailman/mailman-test ,
in that order.
.PP
In any case, if several alias files are supplied,
.B alck
treats them as parts of a single alias file.
.SH OPTIONS
.SS General
These options affect the behavior of the program in general.  They can
be specified anywhere in the command line and affect all options that
follow them.
.TP
\fB\-d\fR \fISPEC\fR
Set debug level.  The \fISPEC\fR can contain one or more of the
following letters:
.RS
.TP
.B y
enable parser debugging;
.TP
.BR l " (the letter " ell ")"
enable lexical analizer debugging.
.RE
.sp
Upper-case variants are also accepted.  Prepending a letter with
a dash (\fB\-\fR) reverts its sense.
.TP
.B \-v
Verbosely report the results.
.SS Local domain names
The distinction between local and remote email addreses allows
.B alck
to detect circular dependencies between aliases.  A local email
is any email that has no domain part, or whose domain part contains
a
.BR "local domain" .
For
.B Sendmail
local domains are those that constitute the
.B w
class.
.PP
The following two options supply local domain names to
.BR alck .
They must appear in the command line before any filenames or
.B \-T
options.
.TP
\fB\-w\fR \fIFILE\fR
Read local domain names from the given
.IR FILE .
The file should list each name on a separate line.  Surrounding
whitespace is allowed.  Empty lines and
comments (lines starting with the
.B #
character) are ignored.
.TP
\fB\-W\fR \fICOMMAND\fR
Run \fICOMMAND\fR and read local domain names from its standard
output.
.B Alck
will consider for inclusion only those lines that begin with
alphanumeric character and don't contain whitespace.  Thus, it is
possible to read the local domain names from the
.B Sendmail
configuration with the following option:
.sp
.nf
.in +2
\-W "echo '$=w' | sendmail -bt | sed \-e '1,2d' \-e '/^>/d"
.in
.fi
.SS Input options
Input options can be interspersed with the input file names.  They are
processed in the order of their appearance.
.TP
\fB\-T\fR \fIFILE\fR
Read names of alias files from \fIFILE\fR.
.TP
.B \-r
By default, the program allows any valid
.B Sendmail
constructs in its input files.  To restrict the input syntax to plain
aliases only, i.e. to prohibit the use of pipes and file redirections, use
the \fB\-r\fR option.  This option affects all file names following
it, until the eventual occurrence of the
.B \-u
option or the end of command line.
.TP
.B \-u
This options cancels the effect of the previous
.B \-r
option.
.SS Informational
The following two options instruct the program to display a certain
kind of information and exit.
.TP
.B \-h
Display a terse help summary.
.TP
.B \-V
Display the program version and copyright statement.
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org>.
.SH COPYRIGHT
Copyright \(co 2013, 2024 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:


